from aiogram.dispatcher.filters.state import State, StatesGroup


class LaunchForm(StatesGroup):
    favorite_food = State()
    favorite_restaurant = State()
    start_search = State()
    next_search = State()
