from app.db import db


class User(db.Model):
    __tablename__ = 'gaz_users'

    id = db.Column(db.Integer(), primary_key=True)
    telegram_id = db.Column(db.Integer())
    username = db.Column(db.Unicode())
    favorite_food = db.Column(db.Unicode())
    favorite_restaurant = db.Column(db.Unicode())
