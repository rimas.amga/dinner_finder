import random

from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.types import (
    KeyboardButton, ReplyKeyboardMarkup, )

from app.constants import (CONTACT_A_PARTNER, LIST_OF_PARTICIPANTS_BTN_TEXT, NEXT_PARTNER,
                           PARTICIPATE_BTN_TEXT)
from app.models import User

CANCEL_BUTTON = KeyboardButton('Отмена')


def get_start_keyboard() -> ReplyKeyboardMarkup:
    start_keyboard = ReplyKeyboardMarkup(resize_keyboard=True)

    participate = KeyboardButton(PARTICIPATE_BTN_TEXT)
    list_of_participants = KeyboardButton(LIST_OF_PARTICIPANTS_BTN_TEXT)
    start_keyboard.add(participate)
    start_keyboard.add(list_of_participants)

    return start_keyboard


def get_cancel_keyboard() -> ReplyKeyboardMarkup:
    cancel_keyboard = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)

    cancel_keyboard.add(CANCEL_BUTTON)

    return cancel_keyboard


def get_search_keyboard(is_search_start: bool = False) -> ReplyKeyboardMarkup:
    search_keyboard = ReplyKeyboardMarkup(resize_keyboard=True)

    if is_search_start:
        search_keyboard.add(KeyboardButton('Начать поиск'))
    else:
        search_keyboard.add(KeyboardButton(NEXT_PARTNER))
        search_keyboard.add(KeyboardButton(CONTACT_A_PARTNER))

    search_keyboard.add(CANCEL_BUTTON)

    return search_keyboard


async def get_new_participant(message: types.Message, state: FSMContext):
    state_data = await state.get_data()

    if user_id := state_data.get('user_id'):
        users_query = User.query.where(User.id != user_id)

        if last_user := state_data.get('last_user'):
            users_query.where(User.telegram_id != last_user)

        users = await users_query.gino.all()
        users_count = len(users)

        if users_count:
            user = users[random.randint(0, users_count - 1)]
            await state.update_data(last_user=user.telegram_id)
            await message.answer(
                f'Пользователь под ником @{user.username}\nЛюбит {user.favorite_food}\n'
                f'Предпочитает {user.favorite_restaurant}\n',
                reply_markup=get_search_keyboard(),
            )
        else:
            await message.answer(
                'Пока никого нет, попробуйте еще раз позже',
                reply_markup=get_start_keyboard(),
            )
            await state.finish()
