import logging

from aiogram import Bot, types
from aiogram.contrib.fsm_storage.redis import RedisStorage2
from aiogram.dispatcher import Dispatcher, FSMContext
from aiogram.dispatcher.filters import Text
from aiogram.utils import exceptions

from app.constants import CONTACT_A_PARTNER, LIST_OF_PARTICIPANTS_BTN_TEXT, NEXT_PARTNER, \
    PARTICIPATE_BTN_TEXT
from app.models import User
from app.states import LaunchForm
from app.utils import get_cancel_keyboard, get_new_participant, get_search_keyboard, \
    get_start_keyboard
from settings import config

logging.basicConfig(level=logging.INFO)

bot = Bot(token=config.BOT_TOKEN)
fsm_storage = RedisStorage2(host=config.REDIS_HOST, port=config.REDIS_PORT, db=config.REDIS_DB_FSM)
dispatcher = Dispatcher(bot, storage=fsm_storage)


@dispatcher.message_handler(commands=['start'])
async def process_start_command(message: types.Message):
    await message.answer(
        text='Привет!\nЯ бот по поиску партнеров для совместных обедов!',
        reply_markup=get_start_keyboard(),
    )


# You can use state '*' if you need to handle all states
@dispatcher.message_handler(state='*', commands=['cancel'])
@dispatcher.message_handler(Text(equals='Отмена', ignore_case=True), state='*')
async def cancel_handler(message: types.Message, state: FSMContext):
    current_state = await state.get_state()
    if current_state is None:
        return

    logging.info('Cancelling state %r', current_state)
    await state.finish()
    await message.answer('Участие отменено.', reply_markup=get_start_keyboard())


@dispatcher.message_handler(Text(equals=PARTICIPATE_BTN_TEXT), state='*')
async def start_participant(message: types.Message, state: FSMContext):
    user = await User.query.where(User.telegram_id == message.from_user.id).gino.first()

    if user:
        await message.answer(
            'Вы уже есть в базе\n'
            f'Любимое блюдо {user.favorite_food}\nПредпочитаемое место {user.favorite_restaurant}'
            '\nПродолжим поиск',
        )
        await state.update_data(user_id=user.id)
        await LaunchForm.next_search.set()
        await get_new_participant(message, dispatcher.get_current().current_state())
    else:
        await LaunchForm.favorite_food.set()
        await message.answer(
            'Отлично! Теперь напишите название любимого блюда',
            reply_markup=get_cancel_keyboard(),
        )


@dispatcher.message_handler(Text(equals=LIST_OF_PARTICIPANTS_BTN_TEXT), state='*')
async def get_participant_list(message: types.Message):
    message_text = '\n---------------------\n'.join(
        f'Пользователь @{user.username}\nЕда: {user.favorite_food}\n'
        f'Место: {user.favorite_restaurant}'
        for user in await User.query.gino.all()
    )
    await message.answer(message_text, reply_markup=get_start_keyboard())


@dispatcher.message_handler(state=LaunchForm.favorite_food)
async def add_favorite_food(message: types.Message, state: FSMContext):
    await state.update_data(favorite_food=message.text)
    await LaunchForm.next()
    await message.answer(
        f'Неплохой выбор {message.text}, теперь опиши место где предпочитаешь проводить обеды',
    )


@dispatcher.message_handler(state=LaunchForm.favorite_restaurant)
async def add_favorite_restaurant(message: types.Message, state: FSMContext):
    await state.update_data(favorite_restaurant=message.text)

    await LaunchForm.next()
    await message.answer(
        text='Теперь можно начать поиск подходящих людей',
        reply_markup=get_search_keyboard(is_search_start=True),
    )


@dispatcher.message_handler(state=LaunchForm.start_search)
async def start_search_participants(message: types.Message, state: FSMContext):
    await message.answer(f'И так начнем поиск...')
    state_data = await state.get_data()
    # Create user model
    user = await User.create(
        telegram_id=message.from_user.id,
        username=message.from_user.username,
        favorite_food=state_data['favorite_food'],
        favorite_restaurant=state_data['favorite_restaurant'],
    )
    await state.update_data(user_id=user.id)
    await LaunchForm.next()
    await get_new_participant(message, state)


@dispatcher.message_handler(Text(equals=NEXT_PARTNER), state=LaunchForm.next_search)
async def next_participants(message: types.Message, state: FSMContext):
    await get_new_participant(message, state)


@dispatcher.message_handler(Text(equals=CONTACT_A_PARTNER), state=LaunchForm.next_search)
async def choose_participants(message: types.Message, state: FSMContext):
    last_user = (await state.get_data()).get('last_user')

    if last_user:
        await message.answer(
            'Мы напишем о вас выбранному человеку, Возможно он тоже выберет вас.',
            reply_markup=get_start_keyboard(),
        )

        try:
            await bot.send_message(
                last_user,
                f'Вас выбрал для обеда @{message.from_user.username}, Можно написать ему',
            )
        except exceptions.ChatNotFound:
            await message.answer('увы но чат с пользователем недоступен')

    else:
        await message.answer(
            'Увы выбор не определен.',
            reply_markup=get_start_keyboard(),
        )

    await state.finish()
