import os

# Bot settings
BOT_TOKEN = os.getenv('BOT_TOKEN')

#db settings
DATABASE_URL = os.getenv('DATABASE_URL', 'postgres://postgres:postgres@localhost:5432/postgres')

# Redis settings
REDIS_HOST = os.getenv("REDIS_HOST", "redis")
REDIS_PORT = os.getenv("REDIS_PORT", 6379)
REDIS_DB_FSM = os.getenv("REDIS_DB_FSM", 0)
