import logging

from aiogram import Dispatcher
from aiogram.utils import executor

from app.bot import dispatcher
from app.db import db
from app.models import User
from settings import config

logging.basicConfig(level=logging.INFO)


async def on_startup(dispatcher: Dispatcher, url=None, cert=None):
    logging.info("Bot Startup")
    logging.info("Setup PostgreSQL Connection")
    await db.set_bind(config.DATABASE_URL)
    await db.gino.create_all()


async def on_shutdown(dispatcher: Dispatcher):
    logging.info("Bot shutdown")
    bind = db.pop_bind()

    if bind:
        logging.info("Close PostgreSQL Connection")
        await bind.close()
    logging.info("Close storage connection")
    await dispatcher.storage.close()
    await dispatcher.storage.wait_closed()


if __name__ == '__main__':
    executor.start_polling(
        dispatcher=dispatcher,
        on_startup=on_startup,
        on_shutdown=on_shutdown,
        skip_updates=True,
    )
