FROM python:3.8.5-slim

RUN mkdir /bot_service && apt-get update && apt-get install -y git libpq-dev postgresql-client
WORKDIR /bot_service

ADD ./requirements.txt /bot_service/requirements.txt

RUN pip install -r requirements.txt
ADD ./ /bot_service
